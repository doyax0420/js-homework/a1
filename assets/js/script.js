function greaterNum(num1, num2){
	if (num1 > num2){
		console.log(num1);
	}else if(num1 < num2){
		console.log(num2);
	}else{
		console.log("the same number");
	}
}

function helloWorld(language){
	switch(language){
		case "Japanese":
			console.log("こんにちは世界");
			break;
		case "Spanish":
			console.log("Hola mundo");
			break;
		case "French":
			console.log("Bonjour monde");
			break;
		default:
			console.log("invalid language");
			break;
		}		
}

function assignGrade(grade){
	if(grade >= 90 && grade <= 100){
		return "Your Grade is A";
	}else if(grade >= 80 && grade <= 89){
		return "Your Grade is B";
	}else if(grade >= 70 && grade <= 79){
		return "Your Grade is C";
	}else if(grade >= 60 && grade <= 69){
		return "Your Grade is D";
	}else if(grade < 60){
		return "Your Grade is F";
	}else{
		return "Please input a number between 0 and 100";
	}
}